import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => (
  <header>
    <nav className="container">
      <Link to="/" className="link">
        <span className="link">Harry Potter Database</span>
      </Link>
    </nav>

    <style jsx>{`
      header {
        background-color: #212529;
        box-shadow: 0 0.25rem 0.75rem rgb(0 0 0 / 5%);
        padding: 1rem 0.75rem;
      }
      .link {
        text-decoration: none;
        color: #fff;
        font-size: 1.25rem;
      }
    `}</style>
  </header>
);

export { Header };
