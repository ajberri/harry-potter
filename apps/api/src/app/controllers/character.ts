import * as express from 'express';
import { characters } from '../../assets/characters';

const getCharacter = (req: express.Request, res: express.Response) => {
  const {
    params: { id },
  } = req;

  const character = characters.find(
    ({ id: characterId }) => characterId === id
  );

  if (!character) {
    res.status(404).send({ error: 'character not found' });
    return;
  }

  res.send(character);
};

export { getCharacter };
