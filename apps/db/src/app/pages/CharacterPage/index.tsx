import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Character } from '@harry-potter/api-interfaces';

import { environment } from '../../../environments/environment';
import { PageContainer } from '../../containers/PageContainer';

const CharacterPage: React.FC = () => {
  const { id } = useParams() as { id: string };
  const [character, setCharacter] = useState<Character>();

  useEffect(() => {
    async function fetchData() {
      const response = await fetch(`${environment.apiUrl}/characters/${id}`);
      const data = (await response.json()) as Character;
      setCharacter(data);
    }

    fetchData();
  }, [id]);

  return (
    <PageContainer>
      {character && (
        <>
          <div className="image container">
            <img src={character.image} alt={character.name} />
          </div>
          <div className="details">
            <div className="container">
              <table>
                <tr>
                  <th>Name:</th>
                  <td>{character.name}</td>
                </tr>
                <tr>
                  <th>Date of birth:</th>
                  <td>{character.dob}</td>
                </tr>
                <tr>
                  <th>Birth place:</th>
                  <td>{character.birthPlace}</td>
                </tr>
                <tr>
                  <th>Nicknames:</th>
                  <td>
                    {Array.isArray(character.nickNames)
                      ? character.nickNames.join(', ')
                      : 'n/a'}
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </>
      )}

      <style jsx>{`
        .image {
          padding: 1.25rem 0;
          text-align: center;
        }
        .image img {
          padding: 0.25rem;
          background-color: #fff;
          border: 1px solid #dee2e6;
          border-radius: 0.25rem;
          margin: 0 auto;
        }
        .details {
          background-color: #f8f9fa;
          padding: 3rem 0;
        }
        table {
          width: 100%;
          border-spacing: 0;
        }
        td,
        th {
          padding: 0.5rem;
          border-bottom: 1px solid #dee2e6;
        }
        th {
          text-align: left;
          width: 20%;
        }
      `}</style>
    </PageContainer>
  );
};

export { CharacterPage };
