import * as express from 'express';
import * as cors from 'cors';
import { Message } from '@harry-potter/api-interfaces';

import { characters } from './assets/characters';
import { getCharacter } from './app/controllers/character';

const app = express();
app.use(cors());

const greeting: Message = { message: '🧙 harry potter api' };

app.get('/', (req: express.Request, res: express.Response) => {
  res.send(greeting);
});

app.get('/characters', (req: express.Request, res: express.Response) => {
  res.send(characters);
});

app.get('/characters/:id', getCharacter);

const port = process.env.port || 3333;
const server = app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}`);
});
server.on('error', console.error);
