import React, { ReactNode } from 'react';

import { Header } from '../../components/Header';
import { Footer } from '../../components/Footer';

type PageContainerProps = {
  children: ReactNode | ReactNode[];
};

const PageContainer: React.FC<PageContainerProps> = (
  props: PageContainerProps
) => {
  const { children } = props;

  return (
    <>
      <Header />
      <main>{children}</main>
      <Footer />
    </>
  );
};

export { PageContainer };
