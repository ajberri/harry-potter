import React from 'react';
import { Link } from 'react-router-dom';

import { Character } from '@harry-potter/api-interfaces';

const CharacterCard: React.FC<Character> = (props: Character) => (
  <div className="card">
    <Link to={`/character/${props.id}`}>
      <img src={props.image} alt={props.name} />
    </Link>

    <div className="details">
      <Link to={`/character/${props.id}`}>{props.name}</Link>
      <span>{props.dob}</span>
    </div>

    <style jsx>{`
      .card {
        display: flex;
        flex-direction: column;
        border: 1px solid rgba(0, 0, 0, 0.125);
        border-radius: 0.25rem;
        background-color: #fff;
      }
      img {
        border-radius: 0.25rem 0.25rem 0 0;
      }
      a {
        color: #333;
      }
      a,
      img {
        width: 100%;
      }
      .details {
        padding: 1rem;
        display: flex;
      }
      .details span {
        text-align: right;
        flex-grow: 1;
      }
    `}</style>
  </div>
);

export { CharacterCard };
