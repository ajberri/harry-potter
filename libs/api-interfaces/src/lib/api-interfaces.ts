export interface Message {
  message: string;
}

export interface Character {
  id: string;
  name: string;
  dob: string;
  birthPlace: string;
  nickNames: string[];
  image: string;
}
