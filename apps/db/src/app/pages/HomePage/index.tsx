import React from 'react';

import { PageContainer } from '../../containers/PageContainer';
import { CharacterList } from '../../containers/CharacterList';

const HomePage: React.FC = () => (
  <PageContainer>
    <div className="intro container">
      <h1>Harry Potter Character Database</h1>
      <p>
        Browse our comprehensive database to out more information about your
        favourite Harry Potter character.
      </p>
    </div>

    <div className="characters">
      <div className="container">
        <CharacterList />
      </div>
    </div>

    <style jsx>{`
      .intro {
        text-align: center;
        padding: 2rem 0.75rem;
      }
      .intro p {
        color: #6c757d;
        font-size: 1.25rem;
        font-weight: 300;
      }
      .characters {
        background: #f8f9fa;
        padding: 1rem 0.75rem;
      }

      @media only screen and (min-width: 768px) {
        .intro {
          padding: 6rem 0.75rem;
        }
        .characters {
          padding: 3rem 0.75rem;
        }
      }
    `}</style>
  </PageContainer>
);

export { HomePage };
