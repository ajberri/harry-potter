import { StrictMode } from 'react';
import * as ReactDOM from 'react-dom';

import App from './app/app';

ReactDOM.render(
  <StrictMode>
    <App />

    <style jsx global>{`
      html {
        box-sizing: border-box;
        font-size: 16px;
        font-family: 'Roboto', sans-serif;
      }

      body {
        margin: 0;
      }

      a {
        text-decoration: none;
        color: #333;
      }

      *,
      *:before,
      *:after {
        box-sizing: inherit;
      }

      div :global(.container) {
        max-width: 1140px;
        margin: 0 auto;
      }
    `}</style>
  </StrictMode>,
  document.getElementById('root')
);
