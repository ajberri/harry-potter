# Harry Potter Character Database

This project was built with [nx](https://nx.dev/). To get started with nx, refer to the [Getting Started](https://nx.dev/l/n/getting-started/intro) guide.

## Getting started

To create a local API development server, run `nx serve api` and for a local frontend app run `nx serve db`.

## Build

Run `nx build db` and `nx build api` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `nx test api` and `nx test db` to execute the unit tests via [Jest](https://jestjs.io).

Run `nx affected:test` to execute the unit tests affected by a change.
