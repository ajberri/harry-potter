import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import { CharacterPage } from './pages/CharacterPage';
import { HomePage } from './pages/HomePage';

export const App = () => (
  <Router>
    <Switch>
      <Route path="/character/:id">
        <CharacterPage />
      </Route>
      <Route path="/">
        <HomePage />
      </Route>
    </Switch>
  </Router>
);

export default App;
