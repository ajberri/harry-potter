import React, { useEffect, useState } from 'react';
import { Character } from '@harry-potter/api-interfaces';

import { CharacterCard } from '../../components/CharacterCard';
import { environment } from '../../../environments/environment';

const CharacterList = () => {
  const [characters, setCharacters] = useState<Character[]>([]);

  useEffect(() => {
    async function fetchData() {
      const response = await fetch(`${environment.apiUrl}/characters`);
      const data = (await response.json()) as Character[];
      setCharacters(data);
    }

    fetchData();
  }, []);

  return (
    <div className="list">
      {characters.length > 0 &&
        characters.map((character) => (
          <div className="card-container" key={character.id}>
            <CharacterCard {...character} />
          </div>
        ))}

      <style jsx>{`
        .card-container {
          width: 100%;
          margin-bottom: 10px;
        }

        @media only screen and (min-width: 768px) {
          .list {
            display: flex;
          }
          .card-container {
            width: 25%;
            margin-left: 10px;
          }
          .card-container:nth-child(4n + 1) {
            margin-left: 0;
          }
        }
      `}</style>
    </div>
  );
};

export { CharacterList };
