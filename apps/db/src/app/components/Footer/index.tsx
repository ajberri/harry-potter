import React from 'react';

const Footer = () => (
  <footer>
    <div className="container">
      <p>Harry Potter Database ©{new Date().getFullYear()}</p>
    </div>

    <style jsx>{`
      footer {
        padding: 1rem 0.75rem;
      }

      @media only screen and (min-width: 768px) {
        footer {
          padding: 3rem 0.75rem;
        }
      }
    `}</style>
  </footer>
);

export { Footer };
